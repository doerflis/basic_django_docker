# base image
FROM python:3.9

# setup environment variable
ENV DockerHOME=/usr/src/code

# where your code lives
WORKDIR $DockerHOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip

# copy project files to your docker home directory.
ADD . $DockerHOME

# run this command to install all dependencies
RUN pip install -r requirements.txt

