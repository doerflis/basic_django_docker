#!/bin/bash
echo "Waiting few seconds for Postgres to come up, so that applying migrations does not fail."
sleep 2.5

echo "Collecting static files for Django ..."
python manage.py collectstatic

echo "Migrations apps from Django ..."
python manage.py migrate

echo "Starting gunicorn ..."
gunicorn basic_django_docker.wsgi -b 0.0.0.0:8080
